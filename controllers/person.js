module.exports = app => {
	return { create, read, update, remove, getAll };

	// Create a new person
	function create(req, res) {
		var newPerson = new app.models.Person(req.body);
		newPerson.save(function(err, result) {
			if (err) {
				res.send(err);
			}
			if (result) {
				res.send(result);
			}
		});
	}

	// Get a person
	function read(req, res) {
		app.models.Person.findOne(req.body, function(err, result) {
			if (err) {
				res.send(err);
			}
			if (result) {
				res.send(result);
			}
		});
	}

	// Update a person
	function update(req, res) {
		app.models.Person.findOneAndUpdate(
			req.body.personToUpdate,
			req.body.update,
			{ new: true },
			function(err, result) {
				if (err) {
					res.send(err);
				}
				if (result) {
					res.send(result);
				}
			}
		);
	}

	// Remove a person
	function remove(req, res) {
		app.models.Person.deleteOne(req.body, function(err, result) {
			if (err) {
				res.send(err);
			}
			if (result) {
				res.send(result);
			}
		});
	}

	// Get all person
	function getAll(req, res) {
		app.models.Person.find({}, function(err, result) {
			if (err) {
				res.send(err);
			}
			if (result) {
				res.send(result);
			}
		});
	}
};
