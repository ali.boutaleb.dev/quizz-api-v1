const express = require('express');
const app = express();
const port = 3000;

// Middleware for parsing json
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Require
require('./models')(app);
require('./controllers')(app);
require('./routes')(app);

app.listen(port, () => console.log(`Example app listening on port port!`));
