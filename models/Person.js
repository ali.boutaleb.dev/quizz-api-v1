var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var personSchema = new Schema({
	firstName: String,
	lastName: String,
	birthDate: Date,
	gender: String,
	email: String
});

module.exports = mongoose.model('Person', personSchema);
