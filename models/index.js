var mongoose = require('mongoose');

// Correct warning for findAndUpdate function
mongoose.set('useFindAndModify', false);

// Connect to database
mongoose.connect('mongodb://localhost/', {
	dbName: 'quizzDatabase',
	useNewUrlParser: true,
	useUnifiedTopology: true
});
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
	console.log('Connected to database');
});

module.exports = app => {
	app.models = {
		Person: require('./Person')
	};
};
