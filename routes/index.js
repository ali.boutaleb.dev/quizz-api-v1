module.exports = app => {
	app.get('/', (req, res) => res.send('Hello World!'));

	app.post('/createPerson', app.controllers.person.create);
	app.post('/readPerson', app.controllers.person.read);
	app.post('/updatePerson', app.controllers.person.update);
	app.post('/removePerson', app.controllers.person.remove);
	app.get('/getAllPerson', app.controllers.person.getAll);
};
